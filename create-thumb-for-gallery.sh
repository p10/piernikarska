#!/usr/bin/env sh

cd img/gallery
gm mogrify -resize 320x240 -quality 100 -output-directory thumbs *.jpg
